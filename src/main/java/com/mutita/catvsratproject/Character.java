/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mutita.catvsratproject;

/**
 *
 * @author Admin
 */
public class Character {
    String name ;
    int HP;
    
    public Character(String name,int HP){
        this.name = name;
        this.HP=HP;
    }
    public void Attack(cat cat1, rat rat1) {
        if(cat1.HP!=0&&rat1.HP!=0){
        if (cat1.HP > rat1.HP) {
            cat1.HP += rat1.HP;
            rat1.HP = 0;
            System.out.println(cat1.name + " is win" + " " + " HP = " + cat1.HP);
            System.out.println(rat1.name + " is loss" + " " + " HP = " + rat1.HP);
            System.out.println("-----------------------------");
        }
        else if(cat1.HP < rat1.HP){
            rat1.HP += cat1.HP;
            cat1.HP = 0;
            System.out.println(cat1.name + " is loss" + " " + " HP = " + cat1.HP);
            System.out.println(rat1.name + " is win" + " " + " HP = " + rat1.HP);
            System.out.println("-----------------------------");
        }
        else{
            cat1.HP = 0;
            rat1.HP = 0;
            System.out.println("equal");
            System.out.println(cat1.name + " HP = " + cat1.HP);
            System.out.println(rat1.name + " HP = " + rat1.HP);
            System.out.println("-----------------------------");
        }
        }
        else{
            System.out.println("Stop");
        }
    }
    
    public void Attack(cat cat1, rat rat1, int i) {
        if(cat1.HP!=0&&rat1.HP!=0){
        if (cat1.HP > 0) {
            if (cat1.HP > rat1.HP||cat1.HP < rat1.HP) {
                //System.out.println("win!!");
                cat1.HP -= i;
                rat1.HP -= i;
                if (rat1.HP == 0) {
                    System.out.println(cat1.name + " is win" + " " + " HP = " + cat1.HP);
                    System.out.println("-----------------------------");
                }else{
                    System.out.println(cat1.name + " HP = " + cat1.HP);
                    System.out.println(rat1.name + " HP = " + rat1.HP);
                    System.out.println("-----------------------------");
                }
            }
             else{
                cat1.HP = 0;
                rat1.HP = 0;
                System.out.println("equal");
                System.out.println(cat1.name + " HP = " + cat1.HP);
                System.out.println(rat1.name + " HP = " + rat1.HP);
                System.out.println("-----------------------------");
        }
        } else {
            System.out.println(cat1.name + " is loss" + " " + " HP = " + cat1.HP);
            System.out.println("-----------------------------");
        }
        
    }else{
            System.out.println("Stop");    
    }
   }

    public void Attack(rat rat1, cat cat1, int i) {
        if(cat1.HP!=0&&rat1.HP!=0){
        if (rat1.HP > 0) {
            if (rat1.HP > cat1.HP||cat1.HP > rat1.HP) {
                cat1.HP -= i;
                rat1.HP -= i;
                if (cat1.HP == 0) {
                    System.out.println(rat1.name + " is win" + " " + " HP = " + rat1.HP);
                    System.out.println("-----------------------------");
                }else{
                    System.out.println(cat1.name + " HP = " + cat1.HP);
                    System.out.println(rat1.name + " HP = " + rat1.HP);
                    System.out.println("-----------------------------");
                }
            } 
             else{
                cat1.HP = 0;
                rat1.HP = 0;
                System.out.println("equal");
                System.out.println(cat1.name + " HP = " + cat1.HP);
                System.out.println(rat1.name + " HP = " + rat1.HP);
                System.out.println("-----------------------------");
        }
        } else {
            System.out.println(rat1.name + " is loss" + " " + " HP = " + rat1.HP);
            System.out.println("-----------------------------");
        }
    }else{
            System.out.println("Stop"); 
        }
    }
    
    @Override
    public String toString(){
        return "name : " + name + "\nHP : " + HP + "\n-------------";
    }
        
}
